;; Copyright (C) 2010 Andreas Rottmann <a.rottmann@gmx.at>

;; Author: Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the new-style BSD license.

;; You should have received a copy of the BSD license along with this
;; program. If not, see <http://www.debian.org/misc/bsd.license>.

#!r6rs

(import (rnrs)
        (rnrs r5rs)
        (rnrs mutable-pairs)
        (only (srfi :13) string-contains)
        (srfi :26 cut)
        (srfi :64 testing)
        (wak private include)
        (wak fmt))

(define (call-with-string-input-port s proc)
  (proc (open-string-input-port s)))

(define (compose f g)
  (lambda args (f (apply g args))))

(define (string-split str splitter)
  (let ((splitter-len (string-length splitter)))
    (let loop ((i 0) (result '()))
      (cond ((string-contains str splitter i)
             => (lambda (pos)
                  (loop (+ pos splitter-len)
                        (cons (substring str i pos) result))))
            (else
             (reverse (cons (substring str i (string-length str))
                            result)))))))

(include-file ((wak fmt private) test-fmt))
